# simpleRgb2cmyk

A bash script converting rgb pdf to cmyk using [Ghostscript](]https://ghostscript.com/). Inspired by [rgb2cmyk](https://gitlab.com/medor/rgb2cmyk) from OSP.

## Dependency

- Ghostcript

## Usage

- Put every pdfs you want to convert in the folder named `rgb`
- Open you terminal and go to `simpleRgb2cmyk` directory: `cd path/to/simpleRgb2cmyk`
- Run the script: `./rgb2cmyk.sh`
- You will find your converted pdfs in the `cmyk` directory.

## Licence

SimpleRgb2cmyk is free software: you can redistribute it and/or modify it under the
terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

SimpleRgb2cmyk is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along with SimpleRgb2cmyk.  If not, see http://www.gnu.org/licenses/.
## Check the output file

With [imagmagick](http://www.imagemagick.org/script/index.php) in the same
folder, run: `identify -verbose cmyk/your.pdf`