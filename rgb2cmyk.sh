#!/bin/bash
for files in rgb/*; do
  file=$(basename "$files")
  filename="${file%.*}"
  ext="${file##*.}"
  echo "$file"
  gs \
    -dPDFX \
    -dNOPAUSE \
    -dBATCH \
    -sDEVICE=pdfwrite \
    -dPDFSETTINGS=/prepress \
    -r300 \
    -dProcessColorModel=/DeviceCMYK \
    -sColorConversionStrategy=CMYK \
    -dDeviceGrayToK=true \
    -dAutoFilterColorImages=true \
    -dAutoFilterGrayImages=false \
    -dDownsampleMonoImages=false \
    -dDownsampleGrayImages=false \
    -dDownsampleColorImages=false\
    -sOutputFile=cmyk/"$filename""_cmyk.""$ext" \
    rgb/"$file"

done
